# Libretile example mod
 this is a example mod for [Libretile](gitlab.com/celleron56/libretile)

 to showcase the new modding API




## Getting started

`
mkdir -p ~/.libretile/mods &&
git clone https://gitlab.com/celleron56/LT-sample_mod &&
mv LT-sample_mod sample_mod
`

now open libretile 
by running 
`
flatpak run io.gitlab.celleron56.libretile
`

once you started a world press "t"
and enter 
`/loadmod `
press send chat message 

in the next input box enter sample and 
press loadmod 

the landscape should instantly go to a post-appocalyptic
fallout like scene

have fun messing around with the modding API


