t = 0
     
def setup():
  global t
  t = 0
  
def replace(item1 , item2):
     for y in range(0  , lt.mapsizey):
        for x in range(0, lt.mapsizex):
          if lt.map.returnTile(x,y ) == item1 :
                lt.map.setTile(x , y , item2 )
     
def main():
    global t
    if t == 0 :
     lt.registertile("scorcheddirt" , "/home/celleron56/.libretile/mods/sample/scorcheddirt.png" , ["buildable"] )
     lt.registertile("dirtywater" , "/home/celleron56/.libretile/mods/sample/dirtywater.png" , ["buildable"] )
     lt.registertile("contaminatedsand" , "/home/celleron56/.libretile/mods/sample/sand.png" , ["buildable"] )     
     t = 1
     replace('grass' , 'scorcheddirt')
     replace('grass2' , 'scorcheddirt')
     replace('grass3' , 'scorcheddirt')
     replace('water' , 'dirtywater')
     replace('sand' ,'contaminatedsand')
